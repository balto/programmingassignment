package no.zeroday.cgichallenge.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import no.zeroday.cgichallenge.persistence.TimeSeries;

import java.util.Date;
import java.util.LinkedHashMap;

public class TimeSeriesOutput {
    @JsonProperty("meter_id")
    private String meterId;

    @JsonProperty("customer_id")
    private String customerId;

    private String resolution;

    private Date from;

    private Date to;

    private LinkedHashMap<Date, Double> values;

    public TimeSeriesOutput() {
    }

    public TimeSeriesOutput(TimeSeries ts) {
        this.customerId = ts.getCustomerId();
        this.meterId = ts.getMeterId();
        this.resolution = "Hour";
        this.from = ts.getFromTime();
        this.to = ts.getToTime();

        this.values = new LinkedHashMap<>();
        ts.getValues().forEach(v -> this.values.put(v.getMeasurementTime(), v.getValue()));
    }

    public String getMeterId() {
        return meterId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getResolution() {
        return resolution;
    }

    public Date getFrom() {
        return from;
    }

    public Date getTo() {
        return to;
    }

    public LinkedHashMap<Date, Double> getValues() {
        return values;
    }
}
