package no.zeroday.cgichallenge.rest;

import no.zeroday.cgichallenge.persistence.MeterValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("customers")
public class CustomerRestController {

    @Autowired
    MeterValueRepository meterValueRepository;

    @GetMapping("/{customerId}/meter-value-sum")
    public ResponseEntity getMeterValueSum(@PathVariable String customerId, @RequestParam(value = "from") @DateTimeFormat(pattern = "yyyy-MM-dd") Date from, @RequestParam(value = "to") @DateTimeFormat(pattern = "yyyy-MM-dd") Date to) {
        if (from.after(to))
            return ResponseEntity.badRequest().body("from can not be after to");

        Double v = meterValueRepository.SumOfValuesByCustomerId(customerId, from, to);

        return ResponseEntity.ok(new CustomerMeterValueOutput(v));
    }

    @GetMapping("/{customerId}/meter-value-sum/{meterId}")
    public ResponseEntity getMeterValueSumForMeter(@PathVariable String customerId, @PathVariable String meterId, @RequestParam(value = "from") @DateTimeFormat(pattern = "yyyy-MM-dd") Date from, @RequestParam(value = "to") @DateTimeFormat(pattern = "yyyy-MM-dd") Date to) {
        if (from.after(to))
            return ResponseEntity.badRequest().body("from can not be after to");

        Double v = meterValueRepository.SumOfValueByCustomerIdAndMeterId(customerId, meterId, from, to);

        return ResponseEntity.ok(new CustomerMeterValueOutput(v));
    }
}
