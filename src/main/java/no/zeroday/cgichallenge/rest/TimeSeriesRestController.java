package no.zeroday.cgichallenge.rest;

import no.zeroday.cgichallenge.helpers.TimeSeriesValidator;
import no.zeroday.cgichallenge.persistence.MeterValue;
import no.zeroday.cgichallenge.persistence.MeterValueRepository;
import no.zeroday.cgichallenge.persistence.TimeSeries;
import no.zeroday.cgichallenge.persistence.TimeSeriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/time-series")
public class TimeSeriesRestController {

    @Autowired
    TimeSeriesRepository timeSeriesRepository;

    @Autowired
    MeterValueRepository meterValueRepository;

    @Autowired
    TimeSeriesValidator validator;

    @GetMapping("")
    public ResponseEntity getAll(@RequestParam(value = "from", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date from, @RequestParam(value = "to", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date to) {
        ArrayList<TimeSeriesOutput> l = new ArrayList<>();

        if (from != null && to != null) {
            if (from.after(to))
                return ResponseEntity.badRequest().body("from can not be after to");

            timeSeriesRepository.findAllByFromTimeIsGreaterThanEqualAndToTimeIsLessThanEqual(from, to).forEach(ts -> l.add(new TimeSeriesOutput(ts)));
        } else if (from != null)
            timeSeriesRepository.findAllByFromTimeIsGreaterThanEqual(from).forEach(ts -> l.add(new TimeSeriesOutput(ts)));
        else if (to != null)
            timeSeriesRepository.findAllByToTimeIsLessThanEqual(to).forEach(ts -> l.add(new TimeSeriesOutput(ts)));
        else
            timeSeriesRepository.findAll().forEach(ts -> l.add(new TimeSeriesOutput(ts)));

        return ResponseEntity.ok(l);
    }

    @PostMapping("")
    public ResponseEntity post(@RequestBody @Valid TimeSeriesInput payload, Errors errors) {
        //Can't run validator if body is missing fields
        if (!errors.hasErrors()) {
            validator.validate(payload, errors);
        }

        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(errors.getAllErrors());
        }

        TimeSeries t = new TimeSeries();
        t.setCustomerId(payload.getCustomerId());
        t.setMeterId(payload.getMeterId());
        t.setFromTime(payload.getFrom());
        t.setToTime(payload.getFrom());

        timeSeriesRepository.save(t);

        for (Map.Entry<Date, Double> e : payload.getValues().entrySet()) {
            MeterValue v = new MeterValue();
            v.setMeasurementTime(e.getKey());
            v.setValue(e.getValue());
            v.setTimeSeries(t);

            meterValueRepository.save(v);
        }

        return new ResponseEntity(HttpStatus.CREATED);

    }

}
