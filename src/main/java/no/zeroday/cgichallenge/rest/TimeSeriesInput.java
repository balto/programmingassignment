package no.zeroday.cgichallenge.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * POJO that represents the JSON input of a time series
 */
public class TimeSeriesInput {

    @JsonProperty("meter_id")
    @NotEmpty(message = "meter_id is required")
    private String meterId;

    @JsonProperty("customer_id")
    @NotEmpty(message = "customer_id is required")
    private String customerId;

    @NotEmpty(message = "resolution is required")
    private String resolution;

    @NotNull(message = "from is required")
    private Date from;

    @NotNull(message = "to is required")
    private Date to;

    @NotEmpty(message = "values can not be empty")
    private LinkedHashMap<Date, Double> values;

    public TimeSeriesInput() {

    }

    public String getMeterId() {
        return meterId;
    }

    public void setMeterId(String meterId) {
        this.meterId = meterId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public LinkedHashMap<Date, Double> getValues() {
        return values;
    }

    public void setValues(LinkedHashMap<Date, Double> values) {
        this.values = values;
    }
}
