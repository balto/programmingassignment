package no.zeroday.cgichallenge.rest;

//POJO for JSON representation of an aggregated meter value for a customer
public class CustomerMeterValueOutput {

    //Need to use Double since the repository might return null if there are no values. Set to 0D in that case
    private Double meterValueSum;

    public CustomerMeterValueOutput(Double meterValueSum) {
        this.meterValueSum = meterValueSum;

        if(meterValueSum == null)
            this.meterValueSum = 0D;
    }

    public Double getMeterValueSum() {
        return meterValueSum;
    }
}
