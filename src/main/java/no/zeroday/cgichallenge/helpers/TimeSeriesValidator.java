package no.zeroday.cgichallenge.helpers;

import no.zeroday.cgichallenge.persistence.TimeSeriesRepository;
import no.zeroday.cgichallenge.rest.TimeSeriesInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Date;
import java.util.Map;

@Component
public class TimeSeriesValidator implements Validator {

    @Autowired
    TimeSeriesRepository timeSeriesRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return TimeSeriesInput.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        TimeSeriesInput t = (TimeSeriesInput) o;

        //Should probably also add a check for conflicting periods

        //By specification each daily payload contains 24 values
        if (t.getValues().size() != 24)
            errors.reject("Values must contain 24 values");

        //Check if any of the values is outside the to and from time
        for (Map.Entry<Date, Double> e : t.getValues().entrySet()) {
            if (e.getKey().before(t.getFrom()))
                errors.reject(String.format("%s is before the time series from time", e.getKey()));
            else if (e.getKey().after(t.getTo()))
                errors.reject(String.format("%s is after the time series to time", e.getKey()));
        }

    }
}
