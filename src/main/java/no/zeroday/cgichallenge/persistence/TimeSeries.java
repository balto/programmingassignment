package no.zeroday.cgichallenge.persistence;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
public class TimeSeries {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long timeSeriesId;

    @NotNull
    private String meterId;

    @NotNull
    private String customerId;

    @NotNull
    private Date fromTime;

    @NotNull
    private Date toTime;

    @OneToMany(mappedBy = "timeSeries")
    private List<MeterValue> values;

    public TimeSeries() {

    }

    public TimeSeries(String meterId, String customerId, Date fromTime, Date toTime) {
        this.meterId = meterId;
        this.customerId = customerId;
        this.fromTime = fromTime;
        this.toTime = toTime;
    }

    public Long getTimeSeriesId() {
        return timeSeriesId;
    }

    public String getMeterId() {
        return meterId;
    }

    public void setMeterId(String meterId) {
        this.meterId = meterId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Date getFromTime() {
        return fromTime;
    }

    public void setFromTime(Date fromTime) {
        this.fromTime = fromTime;
    }

    public Date getToTime() {
        return toTime;
    }

    public void setToTime(Date toTime) {
        this.toTime = toTime;
    }

    public void setTimeSeriesId(Long timeSeriesId) {
        this.timeSeriesId = timeSeriesId;
    }

    public List<MeterValue> getValues() {
        return values;
    }

    public void setValues(List<MeterValue> values) {
        this.values = values;
    }
}
