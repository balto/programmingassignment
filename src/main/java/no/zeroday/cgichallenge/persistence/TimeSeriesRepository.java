package no.zeroday.cgichallenge.persistence;

import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.Optional;

public interface TimeSeriesRepository extends CrudRepository<TimeSeries, Long> {

    Iterable<TimeSeries> findAllByFromTimeIsGreaterThanEqualAndToTimeIsLessThanEqual(Date fromTime, Date toTime);

    Iterable<TimeSeries> findAllByFromTimeIsGreaterThanEqual(Date fromTime);

    Iterable<TimeSeries> findAllByToTimeIsLessThanEqual(Date toTime);
}
