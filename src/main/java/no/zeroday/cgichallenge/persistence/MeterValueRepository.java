package no.zeroday.cgichallenge.persistence;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface MeterValueRepository extends CrudRepository<MeterValue, Long> {

    @Query("Select sum(m.value) from MeterValue m where m.timeSeries.customerId = :customerId and m.measurementTime >= :from and m.measurementTime <= :to")
    Double SumOfValuesByCustomerId(@Param("customerId") String customerId, @Param("from") Date from, @Param("to") Date to);

    @Query("Select sum(m.value) from MeterValue m where m.timeSeries.customerId = :customerId and m.timeSeries.meterId = :meterId and m.measurementTime >= :from and m.measurementTime <= :to")
    Double SumOfValueByCustomerIdAndMeterId(@Param("customerId") String customerId, @Param("meterId") String meterId, @Param("from") Date from, @Param("to") Date to);
}
