# CGI Programmeringsoppgave

Har valgt å løse oppgaven i Java med Spring Boot. Dette for å spare tid og fordi det er relevant i forhold til stillingen. Som database bruker jeg H2, som oppretter filen "TimeSeriesDatabase.mv.db" når man kjører første gang. 

## Endpoints:
### GET /time-series:  
Viser alle registrerte time-series. Kan ta inn query parametre "from" og "to". For å vise alle oppføringer for en uke kan man gjøre f.eks. kalle med
```
GET 127.0.0.1:8080/time-series?from=2018-08-09&to=2018-08-15
```
En dag:
```
GET 127.0.0.1:8080/time-series?from=2018-08-09&to=2018-08-09
```

### POST /time-series: 
Opprett en ny time-series, tar inn JSON payload som spesifisert i oppgaven.
```
POST 127.0.0.1:8080/time-series

Eksempel på forespørsel:
{
    "meter_id": "qwerty",
    "customer_id": "testing",
    "resolution": "Hour",
    "from": "2018-08-09T00:00:00Z",
    "to": "2018-08-09T23:00:00Z",
    "values": { "2018-08-09T01:00:00Z": 1, "2018-08-09T02:00:00Z": 1.1, "2018-08-09T03:00:00Z": 1.2, "2018-08-09T04:00:00Z": 1.3, "2018-08-09T05:00:00Z": 1.4, "2018-08-09T06:00:00Z": 1.5, "2018-08-09T07:00:00Z": 1, "2018-08-09T08:00:00Z": 1.6, "2018-08-09T09:00:00Z": 1.7, "2018-08-09T10:00:00Z": 1.8, "2018-08-09T11:00:00Z": 1.9, "2018-08-09T12:00:00Z": 2.0, "2018-08-09T13:00:00Z": 2.1, "2018-08-09T14:00:00Z": 2.2, "2018-08-09T15:00:00Z": 2.3, "2018-08-09T16:00:00Z": 2.4, "2018-08-09T17:00:00Z": 2.5, "2018-08-09T18:00:00Z": 2.6, "2018-08-09T19:00:00Z": 2.7, "2018-08-09T20:00:00Z": 2.8, "2018-08-09T21:00:00Z": 2.9, "2018-08-09T22:00:00Z": 3.0, "2018-08-09T23:00:00Z": 3.1, "2018-08-09T00:00:00Z": 3.2 }
}
```

### GET /customers/{customerId}/meter-value-sum
Viser summen av alle målinger for kunden med gitt customerId. Kan også ta inn query parametre "from" og "to" tilsvarende som GET /time-series. På denne måten kan man hente ut summen av alle målinger for en kunde på en uke.

Eksempel på resultat:
```
{
    "meterValueSum": 24
}
```

### GET /customers/{customerId}/meter-value-sum/{meterId}
Viser summen av alle målinger for måleren med gitt meterId for gitt kunde. Også her kan man bruke parametre "from" og "to".

Eksempel på resultat:
```
{
    "meterValueSum": 21.23
}
```

## Andre notater
Jeg har lagt inn noe validering, men skulle gjerne hatt tid til å legge inn enda mer. Nå sjekker den at alle feltene er satt, at det er 24 verdier og at ingen av de verdiene har timestamp utenfor "from" og "to". Jeg tenker det hadde vært lurt å sjekke om det allerede finnes en måling for den gitte perioden også.

Andre nødvendige valideringer som ikke er tatt med er sjekk på om kunden finnes, om måleren finnes og om måleren tilhører kunden.

Oppgaven er også løst slik at man må sende inn 24 verdier, men en mer elegant løsning ville vært at man kunne sende inn så mange verdier man ønsker så lenge de ligger innenfor den gitte perioden. Da ville resolution hatt en effekt som f.eks. kunne hatt verdier som "Hour", "Quarter", "Minute" etc.

Som nevnt ble Spring Boot valgt for å spare tid. Spring Boot gir en del ting gratis, som bl.a. validering. Det tar også utrolig kort tid å sette opp enkle endpoints! Spring Data JPA gjør det enkelt å jobbe mot databasen. Jeg har brukt en blanding av Spring Data sin query builder med metodenavn (TimeSeriesRepository.java) og JPQL med @Query (MeterValueRepository.java).

Det var uvant å skrive Java igjen da jeg ikke har gjort det på 1,5 år, men kom heldigvis fort inn i det igjen.